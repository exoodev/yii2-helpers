<?php

namespace yii\helpers;

use yii\helpers\BaseStringHelper;

/**
 * StringHelper class.
 */
class StringHelper extends BaseStringHelper
{
    /**
     * Check If A String Is JSON
     */
    public static function isJson($value) {
        return is_string($value) && is_array(json_decode($value, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }
}
