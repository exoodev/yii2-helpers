<?php

namespace yii\helpers;

use yii\helpers\BaseHtml;
use yii\helpers\ArrayHelper;

/**
 * Html class.
 */
class Html extends BaseHtml
{
    /**
     * @var array list of tag attributes that should be specially handled when their values are of array type.
     * In particular, if the value of the `data` attribute is `['name' => 'xyz', 'age' => 13]`, two attributes
     * will be generated instead of one: `data-name="xyz" data-age="13"`.
     * @since 2.0.3
     */
    public static $dataAttributes = ['data', 'data-ng', 'ng', 'ex'];

    /**
     * This function renders icon.
     *
     * @param string|array $options If $options is array it will be rendered as HTML attributes for the icon, else it will be used as a value of tag attribute 'class'.
     * Each icon has a unique class.
     * @see http://getuikit.com/docs/icon.html
     * @return string the rendering result.
     */
    public static function icon($class, $options = [])
    {
        static::addCssClass($options, $class);
        return static::tag('i', '', $options);
    }

    /**
     * This function renders badge.
     *
     * @param string $text link body. Note that it will be encoded using [[encode()]].
     * @param string|array $options If $options is array it will be rendered as HTML attributes for the icon, else it will be used as a value of tag attribute 'class'.
     * Each badge has a unique class.
     * @see http://getuikit.com/docs/badge.html
     * @return string the rendering result.
     */
    public static function badge($text, $options = [])
    {
        static::addCssClass($options, ['widget' => 'uk-badge']);
        return static::tag('span', static::encode($text), $options);
    }

    /**
     * Generates a tel hyperlink.
     * @param string $text link body. It will NOT be HTML-encoded. Therefore you can pass in HTML code
     * such as an image tag. If this is coming from end users, you should consider [[encode()]]
     * it to prevent XSS attacks.
     * @param string $tel phone. If this is null, the first parameter (link body) will be treated
     * as the phone and used.
     * @param array $options the tag options in terms of name-value pairs. These will be rendered as
     * the attributes of the resulting tag. The values will be HTML-encoded using [[encode()]].
     * If a value is null, the corresponding attribute will not be rendered.
     * See [[renderTagAttributes()]] for details on how attributes are being rendered.
     * @return string the generated mailto link
     */
    public static function tel($text, $tel = null, $options = [])
    {
        $options['href'] = 'tel:' . ($tel === null ? $text : $tel);
        return static::tag('a', $text, $options);
    }

    /**
     * {@inheritdoc}
     */
    public static function tag($name, $content = '', $options = [])
    {
        if (!empty($options['icon'])) {
            $icon = $options['icon'];
            unset($options['icon']);
            $content = $icon . ' ' . $content;
        }

        if (!empty($options['badge'])) {
            $badge = $options['badge'];
            unset($options['badge']);
            $content = $content . ' ' . $badge;
        }

        $types = [
            'text' => 'uk-input',
            'radio' => 'uk-radio',
            'range' => 'uk-range',
            'checkbox' => 'uk-checkbox',
            'submit' => false,
            'file' => false,
        ];
        $tags = [
            'input' => 'uk-input',
            'select' => 'uk-select',
            'textarea' => 'uk-textarea',
        ];

        if (isset($options['type']) && isset($types[$options['type']])) {
            if (!empty($type = $types[$options['type']])) {
                Html::addCssClass($options, $type);
            }
        } elseif (isset($tags[$name])) {
            Html::addCssClass($options, $tags[$name]);
        }

        return parent::tag($name, $content, $options);
    }

    /**
     * Generates a nested list of checkboxes.
     * A checkbox list allows multiple selection, like [[listBox()]].
     * As a result, the corresponding submitted value is an array.
     * @param string $name the name attribute of each checkbox.
     * @param string|array|null $selection the selected value(s). String for single or array for multiple selection(s).
     * @param array $items the data item used to generate the checkboxes.
     *   ```php
     *   ['label' => $label, 'value' => $value, 'items' => [...]];
     *   ```
     * @param array $options options (name => config) for the checkbox list container tag.
     * The following options are specially handled:
     *
     * - encode: boolean, whether to HTML-encode the checkbox labels. Defaults to true.
     *   This option is ignored if `item` option is set.
     * - itemOptions: array, the options for generating the checkbox tag using [[checkbox()]].
     * - item: callable, a callback that can be used to customize the generation of the HTML code
     *   corresponding to a single item in $items. The signature of this callback must be:
     *
     *   ```php
     *   function ($index, $label, $name, $checked, $value)
     *   ```
     *
     *   where $index is the zero-based index of the checkbox in the whole list; $label
     *   is the label for the checkbox; and $name, $value and $checked represent the name,
     *   value and the checked status of the checkbox input, respectively.
     *
     * See [[renderTagAttributes()]] for details on how attributes are being rendered.
     *
     * @return string the generated checkbox nested list
     */
    public static function activeNestedCheckboxList($model, $attribute, $items, $options = [])
    {
        return static::activeListInput('nestedCheckboxList', $model, $attribute, $items, $options);
    }

    /**
     * Generates a nested list of checkboxes.
     * A checkbox list allows multiple selection, like [[listBox()]].
     * As a result, the corresponding submitted value is an array.
     * @param string $name the name attribute of each checkbox.
     * @param string|array|null $selection the selected value(s). String for single or array for multiple selection(s).
     * @param array $items the data item used to generate the checkboxes.
     *   ```php
     *   ['label' => $label, 'value' => $value, 'items' => [...]];
     *   ```
     * @param array $options options (name => config) for the checkbox list container tag.
     * The following options are specially handled:
     *
     * - encode: boolean, whether to HTML-encode the checkbox labels. Defaults to true.
     *   This option is ignored if `item` option is set.
     * - itemOptions: array, the options for generating the checkbox tag using [[checkbox()]].
     * - item: callable, a callback that can be used to customize the generation of the HTML code
     *   corresponding to a single item in $items. The signature of this callback must be:
     *
     *   ```php
     *   function ($index, $label, $name, $checked, $value)
     *   ```
     *
     *   where $index is the zero-based index of the checkbox in the whole list; $label
     *   is the label for the checkbox; and $name, $value and $checked represent the name,
     *   value and the checked status of the checkbox input, respectively.
     *
     * See [[renderTagAttributes()]] for details on how attributes are being rendered.
     *
     * @return string the generated checkbox nested list
     */
    public static function nestedCheckboxList($name, $selection = null, $items = [], $options = [])
    {
        if (substr($name, -2) !== '[]') {
            $name .= '[]';
        }

        self::addCssClass($options, 'uk-list');
        $visibleContent = self::nestedCheckbox($name, $selection, $items, $options);

        if (isset($options['unselect'])) {
            // add a hidden field so that if the list box has no option being selected, it still submits a value
            $name2 = substr($name, -2) === '[]' ? substr($name, 0, -2) : $name;
            $hidden = static::hiddenInput($name2, $options['unselect']);
            unset($options['unselect']);
        } else {
            $hidden = '';
        }

        return $hidden . static::tag('ul', $visibleContent, $options);
    }

    protected static function nestedCheckbox($name, $selection, $items, $options)
    {
        $formatter = ArrayHelper::remove($options, 'item');
        $itemOptions = ArrayHelper::remove($options, 'itemOptions', []);
        $encode = ArrayHelper::remove($options, 'encode', true);
        $result = [];
        $index = 0;

        foreach ($items as $item) {
            $lines = [];
            $checked = $selection !== null &&
                (!ArrayHelper::isTraversable($selection) && !strcmp($item['value'], $selection)
                    || ArrayHelper::isTraversable($selection) && ArrayHelper::isIn($item['value'], $selection));
            if ($formatter !== null) {
                $lines[] = call_user_func($formatter, $index, $item['$label'], $name, $checked, $item['value']);
            } else {
                $lines[] = static::checkbox($name, $checked, [
                    'value' => $item['value'],
                    'label' => $encode ? static::encode($item['label']) : $item['label'],
                ]);
            }
            $index++;

            if (!empty($item['items'])) {
                $lines[] = Html::tag('ul', self::nestedCheckbox($name, $selection, $item['items'], $options));
            }
            $result[] = Html::tag('li', implode("\n", $lines), $itemOptions);
        }

        return implode("\n", $result);
    }
}

