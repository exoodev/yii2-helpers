<?php

namespace exoo\helpers;

use Yii;
use yii\base\BootstrapInterface;

/**
 * Helpers extension bootstrap class.
 */
class Bootstrap implements BootstrapInterface
{
	/**
     * @inheritdoc
     */
	public function bootstrap($app)
	{
		Yii::$classMap['yii\helpers\Html'] = '@exoo/helpers/Html.php';
		Yii::$classMap['yii\helpers\StringHelper'] = '@exoo/helpers/StringHelper.php';
	}
}
